import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef OnCall = void Function(String value);

class TTTWWW extends StatefulWidget {
  TTTWWW({required this.title});

  // Widget子类中的字段往往都会定义为"final"
  //https://www.jianshu.com/p/b4e2a068274f
  final Widget title;


  static const BasicMessageChannel messageChannel =
  BasicMessageChannel("player", StandardMessageCodec());

  _TTTWWWState st = _TTTWWWState();
  @override
  State<TTTWWW> createState() => _TTTWWWState();

  onPlay() {}

  onCallbakc(OnCall onCall) {
    onCall("ssss");
  }

}

class _TTTWWWState extends State<TTTWWW> {
  Future<String> messageHandler(parama) async {
    print('messageHandler ' + parama);
    setState(() {});
    return 'messageHandler flutter return';
  }

  onPlay() async {
    String resp = await messageChannel.send("onPlay");
  }

  onStop() async {
    String resp = await messageChannel.send("onStop");
  }


  static const BasicMessageChannel messageChannel =
      BasicMessageChannel("player", StandardMessageCodec());

  @override
  Widget build(BuildContext context) {
    const String viewType = 'nativeType';
    // Pass parameters to the platform side.
    final Map<String, dynamic> creationParams = <String, dynamic>{};
    creationParams["sss"] = "abc";

    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return const Text('androidandroid????!',
            textDirection: TextDirection.ltr);
      case TargetPlatform.iOS:
        // return const Text('iOSiOS????!', textDirection: TextDirection.ltr);
        UiKitView vvv = const UiKitView(
          viewType: viewType,
          layoutDirection: TextDirection.ltr,
          creationParams: {"sss": "abc"},
          creationParamsCodec: const StandardMessageCodec(),
        );
        Container ctn = Container(
          color: Colors.red,
          child: vvv,
          height: 300,
          width: 300,
          padding: const EdgeInsets.all(1),
        );
        return ctn;
      default:
        return Container(
          color: Colors.red,
          child: const Text("0000"),
          padding: const EdgeInsets.all(30),
          height: 300,
          width: 300,
        );
      // return const Text('ELSEELSE????!', textDirection: TextDirection.ltr);
    }
  }
}
